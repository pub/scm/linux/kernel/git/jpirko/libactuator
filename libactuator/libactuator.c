/*
 *   libactuator.c - Actuator manipulation library
 *   Copyright (C) 2017 Jiri Pirko <jiri@resnulli.us>
 *
 *   This library is free software; you can redistribute it and/or
 *   modify it under the terms of the GNU Lesser General Public
 *   License as published by the Free Software Foundation; either
 *   version 2.1 of the License, or (at your option) any later version.
 *
 *   This library is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *   Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public
 *   License along with this library; if not, write to the Free Software
 *   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdarg.h>
#include <stdint.h>
#include <syslog.h>
#include <string.h>
#include <ctype.h>
#include <errno.h>
#include <assert.h>
#include <linux/genetlink.h>
#include <linux/actuator.h>
#include <libmnl/libmnl.h>
#include <actuator.h>

#include "mnlg.h"

#define ARRAY_SIZE(x) (sizeof(x) / sizeof((x)[0]))
#define ACTUATOR_EXPORT __attribute__ ((visibility("default")))

struct actuator_cb {
	actuator_cb_t cb;
	void *cb_priv;
};

struct actuator {
	void (*log_fn)(struct actuator *actuator, int priority,
		       const char *file, int line, const char *fn,
		       const char *format, va_list args);
	int log_priority;
	struct mnlg_socket *nlg;
	struct actuator_cb cbs[ACTUATOR_EVENT_TYPE_MAX + 1];
};

void actuator_log(struct actuator *actuator, int priority,
		  const char *file, int line, const char *fn,
		  const char *format, ...)
{
	va_list args;

	va_start(args, format);
	actuator->log_fn(actuator, priority, file, line, fn, format, args);
	va_end(args);
}

static inline void __attribute__((always_inline, format(printf, 2, 3)))
actuator_log_null(struct actuator *actuator, const char *format, ...) {}

#define actuator_log_cond(act, prio, arg...)					\
	do {								\
		if (actuator_get_log_priority(act) >= prio)			\
			actuator_log(actuator, prio, __FILE__, __LINE__,		\
				__FUNCTION__, ## arg);			\
	} while (0)

#ifdef ENABLE_LOGGING
#  ifdef ENABLE_DEBUG
#    define dbg(act, arg...) actuator_log_cond(act, LOG_DEBUG, ## arg)
#  else
#    define dbg(act, arg...) actuator_log_null(act, ## arg)
#  endif
#  define info(act, arg...) actuator_log_cond(act, LOG_INFO, ## arg)
#  define warn(act, arg...) actuator_log_cond(act, LOG_WARNING, ## arg)
#  define err(act, arg...) actuator_log_cond(act, LOG_ERR, ## arg)
#else
#  define dbg(act, arg...) actuator_log_null(act, ## arg)
#  define info(act, arg...) actuator_log_null(act, ## arg)
#  define warn(act, arg...) actuator_log_null(act, ## arg)
#  define err(act, arg...) actuator_log_null(act, ## arg)
#endif

static void log_stderr(struct actuator *actuator, int priority,
		       const char *file, int line, const char *fn,
		       const char *format, va_list args)
{
	fprintf(stderr, "libactuator: %s: ", fn);
	vfprintf(stderr, format, args);
	fprintf(stderr, "\n");
}

static int log_priority(const char *priority)
{
	char *endptr;
	int prio;

	prio = strtol(priority, &endptr, 10);
	if (endptr[0] == '\0' || isspace(endptr[0]))
		return prio;
	if (strncmp(priority, "err", 3) == 0)
		return LOG_ERR;
	if (strncmp(priority, "info", 4) == 0)
		return LOG_INFO;
	if (strncmp(priority, "debug", 5) == 0)
		return LOG_DEBUG;
	return 0;
}

/**
 *	actuator_set_log_fn - set logging callback
 *
 *	@act: libactuator library context
 *	@log_fn: function to be called for logging messages
 *
 *	The built-in logging writes to stderr. It can be overridden
 *	by a custom function, to plug log messages into the user's
 *	logging functionality.
 */
ACTUATOR_EXPORT
void actuator_set_log_fn(struct actuator *actuator,
			 void (*log_fn)(struct actuator *actuator, int priority,
					const char *file, int line,
					const char *fn, const char *format,
					va_list args))
{
	actuator->log_fn = log_fn;
	dbg(actuator, "Custom logging function %p registered.", log_fn);
}

/**
 *	actuator_get_log_priority - get logging priority
 *
 *	@act: libactuator library context
 *
 *	Get the current logging priority.
 */
ACTUATOR_EXPORT
int actuator_get_log_priority(struct actuator *actuator)
{
	return actuator->log_priority;
}

/**
 *	actuator_set_log_priority - set logging priority
 *
 *	@act: libactuator library context
 *	@priority: the new logging priority
 *
 *	Set the current logging priority. The value controls which messages
 *	are logged.
 */
ACTUATOR_EXPORT
void actuator_set_log_priority(struct actuator *actuator, int priority)
{
	actuator->log_priority = priority;
}

static int _mnlg_socket_recvfrom(struct actuator *actuator,
				 mnl_cb_t data_cb, void *data)
{
	int err;

	err = mnlg_socket_recvfrom(actuator->nlg, data_cb, data);
	if (err < 0) {
		err(actuator, "actuator netlink answers: %s\n", strerror(-err));
		return err;
	}
	return 0;
}

static int _mnlg_socket_recv_run(struct actuator *actuator,
				 mnl_cb_t data_cb, void *data)
{
	int err;

	err = mnlg_socket_recv_run(actuator->nlg, data_cb, data);
	if (err < 0) {
		err(actuator, "actuator netlink answers: %s\n", strerror(-err));
		return err;
	}
	return 0;
}

static int _mnlg_socket_sndrcv(struct actuator *actuator,
			       const struct nlmsghdr *nlh,
			       mnl_cb_t data_cb, void *data)
{
	int err;

	err = mnlg_socket_send(actuator->nlg, nlh);
	if (err < 0) {
		err(actuator, "Failed to call mnlg_socket_send\n");
		return err;
	}
	return _mnlg_socket_recv_run(actuator, data_cb, data);
}

static int _mnlg_socket_group_add(struct actuator *actuator,
				  const char *group_name)
{
	int err;

	err = mnlg_socket_group_add(actuator->nlg, group_name);
	if (err < 0) {
		err(actuator, "Failed to call mnlg_socket_group_add\n");
		return err;
	}
	return 0;
}

static void _handle_put(struct nlmsghdr *nlh,
			const struct actuator_handle *handle)
{
	mnl_attr_put_strz(nlh, ACTUATOR_ATTR_BUS_NAME, handle->bus_name);
	mnl_attr_put_strz(nlh, ACTUATOR_ATTR_DEV_NAME, handle->dev_name);
	mnl_attr_put_u32(nlh, ACTUATOR_ATTR_INDEX, handle->index);
}

static const enum mnl_attr_data_type actuator_policy[ACTUATOR_ATTR_MAX + 1] = {
	[ACTUATOR_ATTR_BUS_NAME] = MNL_TYPE_NUL_STRING,
	[ACTUATOR_ATTR_DEV_NAME] = MNL_TYPE_NUL_STRING,
	[ACTUATOR_ATTR_INDEX] = MNL_TYPE_U32,
	[ACTUATOR_ATTR_DRIVER_NAME] = MNL_TYPE_NUL_STRING,
	[ACTUATOR_ATTR_TYPE] = MNL_TYPE_U8,
	[ACTUATOR_ATTR_UNITS] = MNL_TYPE_U8,
	[ACTUATOR_ATTR_VALUE] = MNL_TYPE_U32,
	[ACTUATOR_ATTR_MOVE_STATE] = MNL_TYPE_U8,
};

static int actuator_attr_cb(const struct nlattr *attr, void *data)
{
	const struct nlattr **tb = data;
	int type;

	if (mnl_attr_type_valid(attr, ACTUATOR_ATTR_MAX) < 0)
		return MNL_CB_ERROR;

	type = mnl_attr_get_type(attr);
	if (mnl_attr_validate(attr, actuator_policy[type]) < 0)
		return MNL_CB_ERROR;

	tb[type] = attr;
	return MNL_CB_OK;
}

struct actuator_info {
	enum actuator_event_type type;
	struct nlattr *tb[ACTUATOR_ATTR_MAX + 1];
};

ACTUATOR_EXPORT
enum actuator_event_type
actuator_info_event_type(const struct actuator_info *info)
{
	return info->type;
}

ACTUATOR_EXPORT
void actuator_info_handle(const struct actuator_info *info,
			  struct actuator_handle *handle)
{
	handle->bus_name = mnl_attr_get_str(info->tb[ACTUATOR_ATTR_BUS_NAME]);
	handle->dev_name = mnl_attr_get_str(info->tb[ACTUATOR_ATTR_DEV_NAME]);
	handle->index = mnl_attr_get_u32(info->tb[ACTUATOR_ATTR_INDEX]);
}

ACTUATOR_EXPORT
const char *actuator_info_driver_name(const struct actuator_info *info)
{
	return mnl_attr_get_str(info->tb[ACTUATOR_ATTR_DRIVER_NAME]);
}

ACTUATOR_EXPORT
uint8_t actuator_info_type(const struct actuator_info *info)
{
	return mnl_attr_get_u8(info->tb[ACTUATOR_ATTR_TYPE]);
}

ACTUATOR_EXPORT
uint8_t actuator_info_units(const struct actuator_info *info)
{
	return mnl_attr_get_u8(info->tb[ACTUATOR_ATTR_UNITS]);
}

ACTUATOR_EXPORT
bool actuator_info_has_value(const struct actuator_info *info)
{
	return info->tb[ACTUATOR_ATTR_VALUE];
}

ACTUATOR_EXPORT
uint32_t actuator_info_value(const struct actuator_info *info)
{
	return mnl_attr_get_u32(info->tb[ACTUATOR_ATTR_VALUE]);
}

ACTUATOR_EXPORT
bool actuator_info_has_move_state(const struct actuator_info *info)
{
	return info->tb[ACTUATOR_ATTR_MOVE_STATE];
}

ACTUATOR_EXPORT
uint8_t actuator_info_move_state(const struct actuator_info *info)
{
	return mnl_attr_get_u8(info->tb[ACTUATOR_ATTR_MOVE_STATE]);
}

struct actuator_cb_info {
	struct actuator *actuator;
	struct actuator_cb *cbs;
	unsigned int cbs_count;
};

static int actuator_cb(const struct nlmsghdr *nlh, void *data)
{
	struct genlmsghdr *genl = mnl_nlmsg_get_payload(nlh);
	struct actuator_cb_info *cb_info = data;
	struct actuator_info info = {};
	enum actuator_event_type type;
	struct nlattr **tb = info.tb;
	struct actuator_cb *cb;
	uint8_t cmd = genl->cmd;

	mnl_attr_parse(nlh, sizeof(*genl), actuator_attr_cb, tb);
	if (!tb[ACTUATOR_ATTR_BUS_NAME] || !tb[ACTUATOR_ATTR_DEV_NAME] ||
	    !tb[ACTUATOR_ATTR_INDEX] || !tb[ACTUATOR_ATTR_DRIVER_NAME] ||
	    !tb[ACTUATOR_ATTR_TYPE] || !tb[ACTUATOR_ATTR_UNITS])
		return MNL_CB_ERROR;

	switch (cmd) {
	case ACTUATOR_CMD_GET:
		type = ACTUATOR_EVENT_TYPE_GET;
		break;
	case ACTUATOR_CMD_NEW:
		type = ACTUATOR_EVENT_TYPE_NEW;
		break;
	case ACTUATOR_CMD_DEL:
		type = ACTUATOR_EVENT_TYPE_DEL;
		break;
	default: /* Silently ignore the unknowns */
		return MNL_CB_OK;
	}

	if (cb_info->cbs_count <= type)
		return MNL_CB_ERROR;
	info.type = type;
	cb = &cb_info->cbs[type];
	cb->cb(cb_info->actuator, &info, cb->cb_priv);
	return MNL_CB_OK;
}

/**
 *	actuator_get - gets actuator/actuators information
 *
 *	@actuator: libactuator library context
 *	@handle: actuator handle
 *	@cb: per-actuator instance callback
 *	@cb_priv: callback private data
 *
 *	Gets actuator/actuators information. In case the handle is NULL,
 *	all available actuators are processed.
 */
ACTUATOR_EXPORT
int actuator_get(struct actuator *actuator,
		 const struct actuator_handle *handle,
		 actuator_cb_t _cb, void *cb_priv)
{
	uint16_t flags = NLM_F_REQUEST | NLM_F_ACK;
	struct actuator_cb_info cb_info;
	struct actuator_cb cb;
	struct nlmsghdr *nlh;

	cb_info.actuator = actuator;
	cb.cb = _cb;
	cb.cb_priv = cb_priv;
	cb_info.cbs = &cb;
	cb_info.cbs_count = 1;

	if (!handle)
		flags |= NLM_F_DUMP;
	nlh = mnlg_msg_prepare(actuator->nlg, ACTUATOR_CMD_GET, flags);
	if (handle)
		_handle_put(nlh, handle);
	return _mnlg_socket_sndrcv(actuator, nlh, actuator_cb, &cb_info);
}

/**
 *	actuator_move - moves actuator
 *
 *	@actuator: libactuator library context
 *	@handle: actuator handle
 *	@fmt: format for vararg
 *
 *	Moves the actuator movement.
 */
ACTUATOR_EXPORT
int actuator_move(struct actuator *actuator,
		  const struct actuator_handle *handle, const char *fmt, ...)
{
	struct nlmsghdr *nlh;
	va_list ap;

	nlh = mnlg_msg_prepare(actuator->nlg, ACTUATOR_CMD_MOVE,
			       NLM_F_REQUEST | NLM_F_ACK);
	_handle_put(nlh, handle);

	va_start(ap, fmt);
	while (*fmt) {
		switch (*fmt++) {
		case 'v': /* value */
			mnl_attr_put_u32(nlh, ACTUATOR_ATTR_VALUE,
					 va_arg(ap, uint32_t));
			break;
		default:
			err(actuator, "Unknown move argument format\n");
			return -EINVAL;
		}
	}
	va_end(ap);

	return _mnlg_socket_sndrcv(actuator, nlh, NULL, NULL);
}

/**
 *	actuator_stop - stops actuator
 *
 *	@actuator: libactuator library context
 *	@handle: actuator handle
 *	@fmt: format for vararg
 *
 *	Stops the actuator movement.
 */
ACTUATOR_EXPORT
int actuator_stop(struct actuator *actuator,
		  const struct actuator_handle *handle, const char *fmt, ...)
{
	struct nlmsghdr *nlh;
	va_list ap;

	nlh = mnlg_msg_prepare(actuator->nlg, ACTUATOR_CMD_STOP,
			       NLM_F_REQUEST | NLM_F_ACK);
	_handle_put(nlh, handle);

	va_start(ap, fmt);
	while (*fmt) {
		switch (*fmt++) {
		default:
			err(actuator, "Unknown stop argument format\n");
			return -EINVAL;
		}
	}
	va_end(ap);

	return _mnlg_socket_sndrcv(actuator, nlh, NULL, NULL);
}

/**
 *	actuator_eventfd_process - processes event filedesctiptor
 *
 *	@actuator: libactuator library context
 *
 *	Processes event filedesctiptor.
 */
ACTUATOR_EXPORT
void actuator_event_cb_set(struct actuator *actuator,
			   enum actuator_event_type type,
			   actuator_cb_t cb, void *cb_priv)
{
	actuator->cbs[type].cb = cb;
	actuator->cbs[type].cb_priv = cb_priv;
}

/**
 *	actuator_event_process - processes pending events
 *
 *	@actuator: libactuator library context
 *
 *	Processes pending events.
 */
ACTUATOR_EXPORT
int actuator_event_process(struct actuator *actuator)
{
	struct actuator_cb_info cb_info;

	cb_info.actuator = actuator;
	cb_info.cbs = actuator->cbs;
	cb_info.cbs_count = ARRAY_SIZE(actuator->cbs);
	return _mnlg_socket_recvfrom(actuator, actuator_cb, &cb_info);
}

/**
 *	actuator_event_fd_get - gets event filedesctiptor
 *
 *	@actuator: libactuator library context
 *
 *	Gets event filedesctiptor.
 */
ACTUATOR_EXPORT
int actuator_event_fd_get(struct actuator *actuator)
{
	return mnlg_socket_get_fd(actuator->nlg);
}

static void *myzalloc(size_t size)
{
	return calloc(1, size);
}

/**
 *	actuator_init - inits actuator context
 *
 *	@p_actuator: pointer where new library context address will be stored
 *
 *	Allocates and initializes library context, opens generic Netlink socket.
 */
ACTUATOR_EXPORT
int actuator_init(struct actuator **p_actuator)
{
	struct actuator *actuator;
	const char *env;
	int err;

	actuator = myzalloc(sizeof(*actuator));
	if (!actuator)
		return -ENOMEM;
	actuator->log_fn = log_stderr;
	actuator->log_priority = LOG_ERR;
	env = getenv("ACTUATOR_LOG");
	if (env != NULL)
		actuator_set_log_priority(actuator, log_priority(env));

	dbg(actuator, "actuator context %p created", actuator);
	dbg(actuator, "log_priority=%d", actuator->log_priority);

	actuator->nlg = mnlg_socket_open(ACTUATOR_GENL_NAME,
					 ACTUATOR_GENL_VERSION);
	if (!actuator->nlg) {
		err(actuator, "Failed to connect to actuator Netlink");
		err = -errno;
		goto err_mnlg_socket_open;
	}

	err = _mnlg_socket_group_add(actuator, ACTUATOR_GENL_MCGRP_CONFIG_NAME);
	if (err)
		goto err_mnlg_socket_group_add;

	*p_actuator = actuator;
	return 0;

err_mnlg_socket_group_add:
	mnlg_socket_close(actuator->nlg);
err_mnlg_socket_open:
	free(actuator);
	return err;
}

/**
 *	actuator_fini - finishes actuator context
 *
 *	@actuator: libactuator library context
 *
 *	Do library context cleanup.
 */
ACTUATOR_EXPORT
void actuator_fini(struct actuator *actuator)
{
	mnlg_socket_close(actuator->nlg);
	free(actuator);
}
