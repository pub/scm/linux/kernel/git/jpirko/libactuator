/*
 *   actuator.h - Actuator manipulation library
 *   Copyright (C) 2017 Jiri Pirko <jiri@resnulli.us>
 *
 *   This library is free software; you can redistribute it and/or
 *   modify it under the terms of the GNU Lesser General Public
 *   License as published by the Free Software Foundation; either
 *   version 2.1 of the License, or (at your option) any later version.
 *
 *   This library is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *   Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public
 *   License along with this library; if not, write to the Free Software
 *   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef _ACTUATOR_H_
#define _ACTUATOR_H_

#include <stdarg.h>
#include <stdint.h>
#include <stdbool.h>

#ifdef __cplusplus
extern "C" {
#endif

struct actuator;

void actuator_set_log_fn(struct actuator *actuator,
			 void (*log_fn)(struct actuator *actuator, int priority,
					const char *file, int line,
					const char *fn, const char *format,
					va_list args));
int actuator_get_log_priority(struct actuator *actuator);
void actuator_set_log_priority(struct actuator *actuator, int priority);

struct actuator_handle {
	const char *bus_name;
	const char *dev_name;
	uint32_t index;
};

struct actuator_info;

enum actuator_event_type
actuator_info_event_type(const struct actuator_info *info);
void actuator_info_handle(const struct actuator_info *info,
			  struct actuator_handle *handle);
const char *actuator_info_driver_name(const struct actuator_info *info);
uint8_t actuator_info_type(const struct actuator_info *info);
uint8_t actuator_info_units(const struct actuator_info *info);
bool actuator_info_has_value(const struct actuator_info *info);
uint32_t actuator_info_value(const struct actuator_info *info);
bool actuator_info_has_move_state(const struct actuator_info *info);
uint8_t actuator_info_move_state(const struct actuator_info *info);

typedef void (*actuator_cb_t)(struct actuator *actuator,
			      struct actuator_info *info, void *cb_priv);

int actuator_get(struct actuator *actuator,
		 const struct actuator_handle *handle,
		 actuator_cb_t cb, void *cb_priv);
int actuator_move(struct actuator *actuator,
		  const struct actuator_handle *handle, const char *fmt, ...);
int actuator_stop(struct actuator *actuator,
		  const struct actuator_handle *handle, const char *fmt, ...);

enum actuator_event_type {
	ACTUATOR_EVENT_TYPE_GET,
	ACTUATOR_EVENT_TYPE_NEW,
	ACTUATOR_EVENT_TYPE_DEL,
	__ACTUATOR_EVENT_TYPE_MAX,
	ACTUATOR_EVENT_TYPE_MAX = __ACTUATOR_EVENT_TYPE_MAX - 1
};

void actuator_event_cb_set(struct actuator *actuator,
			   enum actuator_event_type type,
			   actuator_cb_t cb, void *cb_priv);
int actuator_event_process(struct actuator *actuator);
int actuator_event_fd_get(struct actuator *actuator);

int actuator_init(struct actuator **p_act);
void actuator_fini(struct actuator *actuator);

#ifdef __cplusplus
} /* extern "C" */
#endif

#endif /* _ACTUATOR_H_ */
